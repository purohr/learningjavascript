### Operadores logicos

true && true //and

true || true  // OR

!true // not


## Combination

2 < 3 && 3 < 4 // → true


Igualmente, es importante que sepas que las operaciones aritméticas tienen precedencia sobre las operaciones de comparación.

2 + 2 < 3 && 10 < 8 * 2
// Primero se hacen las operaciones aritméticas:
// → 4 < 3 && 10 < 16
// Ahora las comparaciones:
// → false && true
// Finalmente:
// → false

## Dos o más operandos

true && true && true // → true


true && true || false // → true
!true && !true // → false
false && true || !true // → false