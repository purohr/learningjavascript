


# comapra dos valores y devuelve un valor boleano:

5 > 3
3 < 5

5 >= 5
4 >= 5
2 <= 5
2 <= 2

5 === 5

2 !== 3 //direfente

'JavaScript' === 'JavaScript' // true
'JavaScript' === 'Java' // false
"JavaScript" !== 'PHP' // true
    `Estoy Aprendiendo JavaScript` === 'Estoy Aprendiendo JavaScript' // true

# ¿Y si usamos el operador > con cadenas de texto ?
    A tiene un valor de 65 y la letra B tiene un valor de 66

'Alfa' > 'Beta' // false
'Omega' > 'Beta' // true
'alfa' > 'Alfa' // true


## Comparando booleanos

true === true // true
true === false // false
false !== false // false

true > false // true
false < true // true
true > true // false
false < false // false