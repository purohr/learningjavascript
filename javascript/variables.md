### Variables

let cartucho = 4


cartucho + 2 // 6


cartucho + 2 // 6

cartucho = cartucho + 8 // 12


## Texto

let text = "hola"


## Constantes const

Las constantes son variables que no pueden ser reasignadas. Para crear una constante, usamos la palabra reservada const:

const PI = 3.1415
Si intentas reasignar el valor de una constante, obtendrás un error:

PI = 3 // -> TypeError: Assignment to constant variable.

const IS_DISABLED = true

## Variables var

A día de hoy, no es recomendable usar var ya que tiene comportamientos extraños que pueden causar errores en tu código.

En una clase posterior te explicaré cuál es la diferencia entre let, const y var además de por qué, siempre que puedas, deberías evitar var.


## El nombre de las variables

let miVariable = 1
let mivariable = 2
miVariable + mivariable // -> 1 + 2

let n = 'Pepe' // ❌ Mal, no es descriptivo
let userName = 'Juan' // ✅ Bien, se entiende

## Convenciones y nomenclaturas

let camelCase = 1
let camelCaseIsCool = 2
let userName = 'midudev'

let snake_case = 1
let snake_case_is_cool = 2
let user_name = 'midudev'

Lo más habitual, y es buena idea, es usarlo en los nombres de archivos. Por ejemplo, mi_archivo.js. Esto es porque algunos sistemas operativos distinguen entre mayúsculas y minúsculas y, por tanto, mi_archivo.js y Mi_archivo.js son dos archivos diferentes.


SCREAMING_CASE es una forma de nombrar que consiste en escribir todas las palabras en mayúsculas y separarlas con guiones bajos. Por ejemplo:

const SCREAMING_CASE = 1
const SCREAMING_CASE_IS_COOL = 2
const USER_NAME = 'midudev'

## null y undefined
 null es un valor que indica algo vacío, mientras que undefined indica algo que no está definido todavía.

La particularidad de estos dos tipos de datos es que cada uno sólo tiene un valor. El tipo null sólo puede tener el valor null y el tipo undefined sólo puede tener el valor undefined.

## La diferencia entre null y undefined

Mientras que null es un valor que significa que algo no tiene valor, undefined significa que algo no ha sido definido. Por ejemplo, si creamos una variable sin asignarle ningún valor, su valor será undefined:

let rolloDePapel // -> undefined

let rolloDePapel = undefined // -> undefined

let rolloDePapel = null


## Comparaciones con null y undefined

null === undefined // -> false

null === null // -> true
undefined === undefined // -> true